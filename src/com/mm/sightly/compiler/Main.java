package com.mm.sightly.compiler;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;

import javax.script.Bindings;
import javax.script.SimpleBindings;

import com.mm.sightly.compiler.utils.CharSequenceJavaCompiler;

import org.apache.sling.scripting.sightly.compiler.CompilationResult;
import org.apache.sling.scripting.sightly.compiler.CompilationUnit;
import org.apache.sling.scripting.sightly.compiler.SightlyCompiler;
import org.apache.sling.scripting.sightly.java.compiler.ClassInfo;
import org.apache.sling.scripting.sightly.java.compiler.JavaClassBackendCompiler;
import org.apache.sling.scripting.sightly.java.compiler.RenderUnit;
import org.apache.sling.scripting.sightly.render.AbstractRuntimeObjectModel;
import org.apache.sling.scripting.sightly.render.RenderContext;
import org.apache.sling.scripting.sightly.render.RuntimeObjectModel;

public class Main {
  public static void main(String[] args) {

    CompilationUnit compilationUnit = readScriptFromClasspath("resources/sightly-expression.html");

    JavaClassBackendCompiler backendCompiler = new JavaClassBackendCompiler();
    SightlyCompiler sightlyCompiler = new SightlyCompiler();
    CompilationResult compile = sightlyCompiler.compile(compilationUnit, backendCompiler);
    ClassInfo classInfo = buildClassInfo("testScript");
    String source = backendCompiler.build(classInfo);
    StringWriter writer = new StringWriter();
    Bindings bindings = new SimpleBindings();
    RenderContext renderContext = buildRenderContext(bindings);
    try {
      render(writer, classInfo, source, renderContext, new SimpleBindings());
      System.out.println(writer.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static ClassInfo buildClassInfo(final String info) {
    return new ClassInfo() {
      @Override
      public String getSimpleClassName() {
        return "" + info;
      }

      @Override
      public String getPackageName() {
        return "org.apache.sling.scripting.sightly.compiler.java";
      }

      @Override
      public String getFullyQualifiedClassName() {
        return "org.apache.sling.scripting.sightly.compiler.java." + info;
      }
    };
  }

  private static RenderContext buildRenderContext(final Bindings bindings) {
    return new RenderContext() {
      @Override
      public RuntimeObjectModel getObjectModel() {
        return new AbstractRuntimeObjectModel() {};
      }

      @Override
      public Bindings getBindings() {
        return bindings;
      }

      @Override
      public Object call(String functionName, Object... arguments) {
        return arguments[0];
      }
    };
  }

  private static void render(StringWriter writer, ClassInfo classInfo, String source,
      RenderContext renderContext, Bindings arguments) throws Exception {
    ClassLoader classLoader = Main.class.getClassLoader();
    CharSequenceJavaCompiler<RenderUnit> compiler = new CharSequenceJavaCompiler<>(classLoader, null);
    Class<RenderUnit> newClass = compiler.compile(classInfo.getFullyQualifiedClassName(), source);
    RenderUnit renderUnit = newClass.newInstance();
    PrintWriter printWriter = new PrintWriter(writer);
    renderUnit.render(printWriter, renderContext, arguments);
  }

  public static CompilationUnit readScriptFromClasspath(final String scriptResource) {
    ClassLoader classloader = Main.class.getClassLoader();
    InputStream resourceAsStream = classloader.getResourceAsStream(scriptResource);
    final Reader reader = new InputStreamReader(resourceAsStream);
    return new CompilationUnit() {
      @Override public String getScriptName() {
        return scriptResource;
      }

      @Override public Reader getScriptReader() {
        return reader;
      }
    };
  }
}
